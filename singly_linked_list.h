#pragma once

// rule of 3 -> destructor, copy constructor, operator=

template <typename T>
class SinglyLinkedList
{
private:
	struct Node
	{
		T value;
		Node* next;
	};

	Node* head;
	std::size_t list_size;

	void copy(Node* ptr)
	{
		Node** ptr_this = &head;
		for (; ptr != nullptr; ptr = ptr->next)
		{
			*ptr_this = new Node{ ptr->value };
			ptr_this = &((*ptr_this)->next);
		}
	}
public:
	SinglyLinkedList() : head(nullptr), list_size(0)
	{
	}

	~SinglyLinkedList()
	{
		std::cout << "DESTRUCTOR CALL\n";
		while (head != nullptr)
		{
			Node* current = head;
			head = head->next;
			delete current;
		}
	}

	void push_front(T value)
	{
		Node* newNode = new Node{ value, head };
		head = newNode;
		++list_size;
	}

	friend std::ostream& operator<<(std::ostream& out, const SinglyLinkedList& sll)
	{
		Node* current = sll.head;
		while (current != nullptr)
		{
			out << current->value << " ";
			current = current->next;
		}

		return out;
	}

	SinglyLinkedList(const SinglyLinkedList<T>& other)
		: head(nullptr)
		, list_size(other.list_size)
	{
		copy(other.head);
	}

	SinglyLinkedList<T>& operator=(const SinglyLinkedList<T>& other)
	{
		if (this == &other)
			return *this;
		this->~SinglyLinkedList();
		head = nullptr;
		list_size = other.list_size;
		copy(other.head);
		return *this;
	}

	std::size_t size() const
	{
		return list_size;
	}

	void pop_front()
	{
		if (head == nullptr)
			throw std::exception{ "List size is 0, you can't do pop_front" };
		Node* temp = head;
		head = head->next;
		delete temp;
		--list_size;
	}

	void push_back(const T& value)
	{
		Node** ptr = &head;

		while (*ptr != nullptr)
			ptr = &((*ptr)->next);

		*ptr = new Node{ value };
		++list_size;
	}

	void pop_back()
	{
		if (head == nullptr)
			throw std::exception{ "List size is 0, you can't do pop_back" };

		Node** ptr = &head;

		while ((*ptr)->next != nullptr)
			ptr = &((*ptr)->next);

		delete* ptr;
		*ptr = nullptr;
		--list_size;
	}

	// reverse()
};