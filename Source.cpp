#include <iostream>
#include <vector>
#include <algorithm>
#include <chrono>
#include <thread>

namespace horsewalk
{
using Cell = std::pair<std::size_t, std::size_t>;
using CellDiff = std::pair<int, int>;
using Path = std::vector<Cell>;
using VisitMatrix = std::vector<std::vector<bool>>;

class ChessHorsePathFinder
{
public:
    ChessHorsePathFinder(std::size_t rows, std::size_t columns)
        : rows(rows)
        , columns(columns)
    {
    }

    Path find(const Cell& cell) const
    {
        Path path;
        std::size_t cellsLeft = rows * columns;
        VisitMatrix visited(rows, std::vector<bool>(columns));

        --cellsLeft;
        visited[cell.first][cell.second] = true;
        if (find(cell, path, cellsLeft, visited)) 
            return path;
        return {};
    }
private:
    std::vector<CellDiff> horseCellDiffs(const Cell& cell) const
    {
        CellDiff unit{ 1, 2 };
        std::vector<CellDiff> cell_diffs;

        for (int first_sign : {1, -1})
        {
            for (int second_sign : {1, -1})
            {
                auto first = unit.first * first_sign;
                auto second = unit.second * second_sign;
                cell_diffs.push_back({ first, second });
                cell_diffs.push_back({ second, first });
            }
        }

        return cell_diffs;
    }

    std::vector<Cell> getNeighbours(const Cell& cell, const VisitMatrix& visited) const
    {
        std::vector<Cell> neighbours;
        for (const auto& [d_row, d_column] : horseCellDiffs(cell))
        {
            if (d_row < -cell.first || d_column < -cell.second)
                continue;
            Cell neighbour{ cell.first + d_row, cell.second + d_column };
            if (neighbour.first < rows && neighbour.second < columns
                && !visited[neighbour.first][neighbour.second])
                neighbours.push_back(neighbour);
        }
        return neighbours;
    }

    bool find(const Cell& cell, Path& path, std::size_t& cellsLeft, VisitMatrix& visited) const
    {
        if (cellsLeft == 0)
            return true;

        path.push_back(cell);
        std::vector<Cell> neighbours = getNeighbours(cell, visited);

        auto comparator = [this, visited](const Cell& cell1, const Cell& cell2)
        {
            return (this->getNeighbours(cell1, visited)).size() > 
                (this->getNeighbours(cell2, visited)).size();
        };
        std::make_heap(neighbours.begin(), neighbours.end(), comparator);

        while(!neighbours.empty())
        {
            const Cell& neighbour = neighbours[0];
            visited[neighbour.first][neighbour.second] = true;
            --cellsLeft;
            if (find(neighbour, path, cellsLeft, visited))
                return true;
            ++cellsLeft;
            visited[neighbour.first][neighbour.second] = false;
            std::pop_heap(neighbours.begin(), neighbours.end(), comparator);
            neighbours.pop_back();
        }
        path.pop_back();

        return false;
    }

private:
    const std::size_t rows;
    const std::size_t columns;
};

} // namespace chpf


int main()
{
    auto now1 = std::chrono::system_clock::now();

    horsewalk::ChessHorsePathFinder path_finder(9, 9);
    for (const auto& x : path_finder.find({ 4, 4 }))
        std::cout << x.first << " " << x.second << "\n";

    auto now2 = std::chrono::system_clock::now();
    std::cout << now2 - now1;
    return 0;
}
